var express = require('express');
var app = express();
var knex = require('../db');
var port = process.env.PORT || 3000;
var Promise = require("bluebird");

var common = require('./cleanups');

// knex('ingredients')
// .where('id', 100)
// .update({ name_lv_normalised: 'test' })
// .then();

normalise_ingredients()
.then((affected) => {
    // console.log('Ingredients normalised: ' + affected);
});
normalise_synonyms().then((affected) => {
    // console.log('Synonyms normalised: ' + affected);
});

// Promise.all([normalise_ingredients(), normalise_synonyms()])
//     .then(() => console.log('Done'));

function normalise_ingredients() {
    return knex("ingredients")
        .select("name_lv as name", "id")
        .map(function (ingredient) {
            var normalised_name = common.normaliseForDatabase(ingredient.name);
            // console.log('Before ' + ingredient.name);
            // console.log('After ' + normalised_name);
            return knex('ingredients').where('id', ingredient.id).update({ name_lv_normalised: normalised_name });
        });
};

function normalise_synonyms() {
    return knex("synonyms")
        .select("synon_name_lv as name", "id")
        .map(function (ingredient) {
            var normalised_name = common.normaliseForDatabase(ingredient.name);
            return knex('synonyms').where('id', ingredient.id).update({ synon_name_lv_normalised: normalised_name });
        });
};
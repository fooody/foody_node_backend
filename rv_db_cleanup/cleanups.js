var cleanups =
    {
        normaliseForDatabase: function (name) {
            name = name.toLowerCase();
            name = this.removeWhiteSpaces(name);

            name = name.replace(/[;'-\.\*/]/g, ''); //remove all spaces,  semicolons, ' and - and . and /  and *         

            //replace all latvian letters  
            name = this.removeLatvianLetters(name);

            return name;
        },

        removeLatvianLetters: function (str) {
            var name = str.replace(/ā/g, 'a');
            name = name.replace(/ē/g, 'e');
            name = name.replace(/ī/g, 'i');
            name = name.replace(/ū/g, 'u');
            name = name.replace(/š/g, 's');
            name = name.replace(/č/g, 'c');
            name = name.replace(/ģ/g, 'g');
            name = name.replace(/ķ/g, 'k');
            name = name.replace(/ļ/g, 'l');
            name = name.replace(/ņ/g, 'n');
            name = name.replace(/ž/g, 'z');

            return name;
        },


        removePercentageNumber: function (name) {
            name = name.replace(/\(?\d+(?:\.\d+)? ?%\)? */g, "").trim();
            return name;
        },

        removeDoubleSpaces: function (name) {
            name = name.replace(/\s\s+/g, ' '); // Given that you also want to cover tabs, newlines, etc
            return name;
        },

        removeWhiteSpaces: function (name) {
            name = name.replace(/\s/g, '');
            return name;
        },


        removeSpecialWord: function (name, specialWord) {
            var flags = "gi";
            // todo: šo varētu uzlabot, jo dažreiz gadās izlaistas garumzīmes
            // ja mēs aizvietotu latviešu burtus un tad veiktu meklēšanu, taču nedrīkst pazaudēt latviešu burtus no name, kas tiek atgriezts
            // iespējams var veikt advanceto meklēšanu, distance vai tml.


            // var normSpecialWord = this.removeLatvianLetters(specialWord);
            // var normName = this.removeLatvianLetters(name);

            var reg = new RegExp(specialWord + 's?i? ?-?', flags); //antioksidants; antioksidanti;krāsvielas;antioksidants- u.tml.

            return name.replace(reg, "");
        },

        replaceNumbersCommasToPeriods: function (str) {
            //var str = str.replace(/([ $(]\d+),(\d+[ ,%)])/g, "$1.$2");
            var str = str.replace(/(\d+),(\d+)/g, "$1.$2", ".");
            return str;
        },

        matchEnumbersOrLeaveStr: function (str) {
            var matches = str.match(/e ?\d{3,4}[a-f]?i{0,3}/g);

            // remove space between e and number
            if (matches !== null) {
                for (let i = 0; i < matches.length; i++) {
                    matches[i] = matches[i].replace(" ", "");
                }
            }            
            return matches == null ? [str] : matches;
        },

        matchVitaminsOrLeaveStr: function (str) {
            var matches = str.match(/b ?\d{1,2}/g); // only b-vitamins here

            // remove space between b and number
            if (matches !== null) {
                for (let i = 0; i < matches.length; i++) {
                    matches[i] = matches[i].replace(" ", "");
                }
            }            
            return matches == null ? [str] : matches;
        },

        replaceAllBracketsToRounded: function (str) {
            var str = str.split(/[\{\[]/).join('(').split(/[\}\]]/).join(')');
            return str;
        },

        replaceSpaceBeforePercentage: function (str) {
            // (\d) Capture a digit into capture group 1
            // + Match one or more spaces
            // (?=%) Positive lookahead ensuring what follows is a %
            const regex = /(\d) +(?=%)/g;
            const subst = `$1`;
            str = str.replace(regex, subst);
            return str;
        },

        matchDigitsOrChars: function (str) {
            // digits or characters, including whitespaces
            const regex = /^[a-zA-Z0-9\u00C0-\u017F\s]+$/gi
            var matches = str.match(regex);
            return matches == null ? [] : matches;
        },

        matchDigitsOrCharsAndPercSign: function (str) {
            // digits or characters, including whitespaces and percentage
            const regex = /^[a-zA-Z0-9\u00C0-\u017F\s%]+$/gi
            var matches = str.match(regex);
            return matches == null ? [] : matches;
        },

        matchChars: function (str) {
            //  characters, including whitespaces
            const regex = /^[a-zA-Z\u00C0-\u017F\s]+$/g;
            var matches = str.match(regex);
            return matches == null ? [] : matches;
        }


    };
module.exports = cleanups;
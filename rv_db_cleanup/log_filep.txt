{ method: 'select',
  options: {},
  timeout: false,
  cancelOnTimeout: false,
  bindings: [ 5919 ],
  __knexQueryUid: '6bc86528-3f47-4b7e-a315-2cee6525806e',
  toNative: [Function: toNative],
  sql: 'select `products`.`id` as `id`, `products`.`name_lv` as `name`, `products`.`ingredient_description_lv` as `description` from `products` where `id` = ?' }
5919
(1) Name = vājpiens| for Product VĀJPIENS 0,5%| with id = 5919
 Before Norma = vājpiens
 Norma = vajpiens
{ method: 'update',
  options: {},
  timeout: false,
  cancelOnTimeout: false,
  bindings: [ '', 5919 ],
  __knexQueryUid: 'a288b118-4dcd-4b72-98e9-d6e67070b055',
  toNative: [Function: toNative],
  sql: 'update `products` set `var_saturet_text` = ? where `id` = ?' }
{ method: 'select',
  options: {},
  timeout: false,
  cancelOnTimeout: false,
  bindings: [ 'vajpiens' ],
  __knexQueryUid: '8dd63bf9-441c-4a06-b306-ad0cf8e363f9',
  toNative: [Function: toNative],
  sql: 'select `id` from `ingredients` where `name_lv_normalised` = ?' }
{ method: 'select',
  options: {},
  timeout: false,
  cancelOnTimeout: false,
  bindings: [ 'vajpiens' ],
  __knexQueryUid: 'a357590b-dfb2-4288-ac84-38936ef36f5d',
  toNative: [Function: toNative],
  sql: 'select `ingredient_id` as `id` from `synonyms` where `synon_name_lv_normalised` = ?' }
  (2) Ingredient_get for name = vājpiens found id1 = 2375 and id2 = null
{ method: 'raw',
  sql: 'INSERT INTO product_ingredients (ingredient_id, product_id) SELECT ?, ? WHERE NOT EXISTS(SELECT 1 FROM product_ingredients WHERE ingredient_id = ? and product_id = ?)',
  bindings: [ 2375, 5919, 2375, 5919 ],
  options: {},
  __knexQueryUid: 'ae513e09-5a7d-4d0b-aa5a-171774e2f71e' }

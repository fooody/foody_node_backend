var express = require('express');
var app = express();
var knex = require('../db');
var port = process.env.PORT || 3000;
var Promise = require("bluebird");

var fs = require('fs'),
    request = require('request');

// download file
var download = function (uri, filename, callback) {
    request.head(uri, function (err, res, body) {
        // console.log('content-type:', res.headers['content-type']);
        // console.log('content-length:', res.headers['content-length']);

        request(uri).pipe(fs.createWriteStream(filename))            
            .on('error', () => {
                console.log('Err');
            })
            .on('close', callback);
    })

};

knex("products")
    .select("products.id as id", "products.name_lv as name", "products.external_img as img", "products.barcode as barcode")
    // .whereNotExists(function () {
    //     this.select('*').from('product_ingredients').whereRaw('products.id = product_ingredients.product_id');
    // })
    //.whereNull('rating')
    // .where("id", 48)
    //.whereRaw("length(products.ingredient_description_lv) >= 100 and length(products.ingredient_description_lv) < 200")
    .whereRaw("products.id >= 300 and products.id <= 400")
    .mapSeries(function (product) {
        // console.log(product);
        var imgName = 'imgs/' + product.barcode.toString() + ".png";
        // console.log(product.external_img);
        download(product.img, imgName, function () {
            console.log(product.id);
            knex("products").where("id", product.id).update("img_path", imgName).then(() => {

            });
        });
    })
    .then(() => {
        // console.log('done');
    });
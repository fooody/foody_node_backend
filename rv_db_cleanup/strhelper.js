var strhelper =
    {
        isEmptyOrSpaces: function (str) {
            return str === null || str.match(/^ *$/) !== null;
        }
    };
module.exports = strhelper;
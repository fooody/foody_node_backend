var express = require('express');
var app = express();
var knex = require('../db');
var port = process.env.PORT || 3000;
var Promise = require("bluebird");
// var Treeize = requigre("treeize");

var algoHelper = require('../algo_helper');


// algoHelper.update_product_rating(14)
//     .then((rating) => {
//         console.log(rating);
//     });
// update_product_rating(15)
//     .then((affected) => {
//         console.log(affected);
//     });
// update_product_rating(16)
//     .then((affected) => {
//         console.log(affected);
//     });

calculate_all_product_rating();

function calculate_all_product_rating() {
    // run calculate_rating in loop
    knex("products")
        .select(
        "products.id as id", "products.name_lv as name", "products.ingredient_description_lv as description"
        )
        // .whereNotExists(function () {
        //     this.select('*').from('product_ingredients').whereRaw('products.id = product_ingredients.product_id');
        // })        
        // .whereRaw("length(products.ingredient_description_lv) < 40")
        // .whereRaw("products.id < 100")
        //.where("id", 739)
        .mapSeries(function (product) {
            algoHelper.update_product_rating(product.id)
            .then((rating) => {
                // console.log(product.id + ' = ' + rating);
            });
        });
}


var express = require('express');
var app = express();
var knex = require('../db');
var port = process.env.PORT || 3000;
var Promise = require("bluebird");
// var Treeize = requigre("treeize");

var cleanups = require('./cleanups');
var strhelper = require('./strhelper');

// This is Node.js app for create product - ingredient intermediate table (starptabula)
// For now this doens't calculate ratings, this is done by seperate method
// For now we take products for which intermediate table is empty && rating is null


fill_product_ingredient_intermediate_table();

// var wordsToProcess = ['e301', 'e260'];
// var actions = wordsToProcess.map((str) => {
//     console.log(str, '     | ONLY CHARS');
//     // var normalised_name = cleanups.normaliseForDatabase(desc);
//     return ingredient_get(str, str)
//         .then((ingredient_id) => {            
//             return ingredient_id;
//         });
// });
// var results = Promise.all(actions);
// results.then(ingredient_id => {    
//     process.exit(0);
// });


// calculate_product_rating(13);

var fn = function processDescriptionPart(desc, product_id) {
    return new Promise(resolve => {
        // assume no brackets
        desc = desc.trim();
        desc = desc.replace(/\*/g, ''); //just replace asterisk

        // process name with number% without brackets at the end 


        // process name with (number%) at the end

        if (strhelper.isEmptyOrSpaces(desc)) {
            // console.log('');
            //resolve(null);
        } else {
            if (cleanups.matchChars(desc).length > 0) {
                // check if it has one or more e-numbers in it
                // if it has enumbers, then take out only enumbers and process each e-number seperately
                // possible cases: "e301", "e120 e250", "e120 e250", "e415 karagināns", "e407a", "e451i", "biezinatajs e407", "e250 garšvielas"
                // var wordsToProcess = cleanups.matchEnumbersOrLeaveStr(desc); //usually wordsToProces would contain only one value, but in some cases multiple (like in "e120 e250" case)
                // console.log('ingr ', wordsToProcess);

                //  var actions = wordsToProcess.map((str) => {
                //      console.log(desc, '     | ONLY CHARS');
                //      // ingredient_get(normalised_name, str);
                //  });
                //  var results = Promise.all(actions);
                //  results.then(ingredient_id => {
                //      console.log('ingr ', ingredient_id);
                //  });

                // resolve(desc);
            }
            else if (cleanups.matchDigitsOrChars(desc).length > 0) {
                // DIGS + CHARS

                // check if it has one or more e-numbers in it
                // if it has enumbers, then take out only enumbers and process each e-number seperately
                // possible cases: "e301", "e120 e250", "e120 e250", "e415 karagināns", "e407a", "e451i", "biezinatajs e407", "e250 garšvielas"

                var wordsToProcess = cleanups.matchEnumbersOrLeaveStr(desc);
                if (wordsToProcess == desc) {
                    wordsToProcess = cleanups.matchVitaminsOrLeaveStr(desc);
                }

                // usually wordsToProcess would contain only one value, but in some cases multiple (like in "e120 e250" case) 
                // loop through wordsToProcess
                var actions = wordsToProcess.map((str) => {
                    var normalised_name = cleanups.normaliseForDatabase(str);
                    return ingredient_get(normalised_name, str)
                        .then((ingredient_id) => {
                            // console.log(str,' ', str.length, '     | ONLY CHARS. | Found ingredient_id = ',  ingredient_id);
                            if (ingredient_id == null) {
                                console.log(str, '     | ONLY CHARS | For product id = ', product_id, ' |Not found ingredient_id = ', ingredient_id);
                                return ingredient_id;
                            } else {
                                // var rawStr = "INSERT INTO product_ingredients (ingredient_id, product_id) SELECT ?, ? WHERE NOT EXISTS(SELECT 1 FROM product_ingredients WHERE ingredient_id = ? and product_id = ?)";
                                // return knex.raw(rawStr, [ingredient_id, product_id, ingredient_id, product_id])
                                // .on('query', function (data) {
                                //      console.log("ON-QUERY data:", data);
                                //  });
                                return knex('product_ingredients').select()
                                    .where('product_id', product_id)
                                    .where('ingredient_id', ingredient_id)
                                    .then(function (rows) {
                                        console.log('INSERT would be run for ingredient_id = ', ingredient_id, '  and product_id = ', product_id);
                                        console.log(rows.length);
                                        if (rows.length === 0) {
                                            // no matching records found                                            
                                            return knex('product_ingredients').insert({ 'ingredient_id': ingredient_id, 'product_id': product_id })
                                        } else {
                                            // return or throw - duplicate name found
                                            return;
                                        }
                                    })
                                    .catch(function (ex) {
                                        // you can find errors here.
                                    })
                            }
                            // return ingredient_id;
                        });
                });
                var results = Promise.all(actions);
                results.then(ingredient_id => {
                    // console.log('ingr ', ingredient_id);
                });

                // resolve(desc);
            }
            else if (cleanups.matchDigitsOrCharsAndPercSign(desc).length > 0) {
                // console.log(desc, '     | DIGS + CHARS + %');

                // //     resolve(null);
            }
            else {
                // console.log(desc, '     | NO MATCHES');
                //     //resolve(null);
            }
        }
        resolve(desc);

        // if (no_brackets(desc)) {
        //     // console.log('here', desc);
        //     resolve( withNoBracketsDo(desc));
        // } else {
        //     console.log('brackets');
        //     resolve('brackets');
        // }
    });
}


//TODO 0: "The pool is probably full" happens when run on 7000 products
function fill_product_ingredient_intermediate_table() {
    // First step: divide all descriptions into ingredients
    knex("products")
        .select(
        "products.id as id", "products.name_lv as name", "products.ingredient_description_lv as description"
        )
        // .whereNotExists(function () {
        //     this.select('*').from('product_ingredients').whereRaw('products.id = product_ingredients.product_id');
        // })
        //.whereNull('rating')
        // .where("id", 48)
        //.whereRaw("length(products.ingredient_description_lv) >= 100 and length(products.ingredient_description_lv) < 200")
        .whereRaw("products.id >= 20 and products.id < 1000")
        .mapSeries(function (product) {
            //TODO 1: everything that is in this block, should be moved to seperate function with name "processProductIngredients(description)"
            //TODO 2: when all processProductIngredients is finished (including all async tasks in it), algorithm should run method calculate_rating(product) -- see at the end
            //TODO 3: when there is any error in algorithm, the algorithm should not stop, but should accumulate all errors into some structured and console.log together with "FINISH"
            // console.log(product.id);

            // replace all brackets to round brackets
            var desc = cleanups.replaceAllBracketsToRounded(product.description);

            // remove unnecessary text from beginning
            desc = desc.replace(/^sastāvdaļas:?/i, "");
            desc = desc.replace(/^sastāvs:? /i, "");

            desc = cleanups.replaceSpaceBeforePercentage(desc);
            desc = clean_VarSaturet(product.id, desc);

            desc = desc.toLowerCase();
            desc = desc.replace(/[;]/gi, ','); //replace ; with commas

            desc = cleanups.replaceNumbersCommasToPeriods(desc);

            desc = removeSpaceAfterBeforeBrackets(desc);

            // process sojas lecitīni (need to do before "t.sk." as sometimes sojas lecitīni contains t.sk.)
            desc = process_SojasLecitini(desc);

            // remove "t.sk." text
            desc = removeTSk(desc);

            // remove e-number categories and similar special words
            desc = cleanAndSaveSpecialWords(desc);

            desc = cleanups.removeDoubleSpaces(desc);
            desc = desc.trim();

            // replace spaces between number and  percenteage                       
            // var r = /\(?\d+(?:\.\d+)? ?%\)? */g;
            // var tmp = desc.match(r);
            // if (tmp) {
            //     console.log(tmp);
            // }
            // TODO: check if parentheses are balanced
            // if (!isBalanced(product.description)) {
            //     console.log('not balanced');
            // }
            // TODO: check if parentheses are not nested
            // if (nestedParentheses(product.description)) {
            //    console.log('nested');
            // }
            // var desc = descriptionNormalise(product.id, desc);
            // console.log(desc);

            var parts = p(desc);

            // console.log('1 -------------');
            // console.log(parts);
            // console.log('2 -------------');
            var actions = parts.map((data) => fn(data, product.id));

            // we now have a promises array and we want to wait for it
            var results = Promise.all(actions);

            results.then(data => { // or just .then(console.log)
                // console.log(data);
            });
        })
        .then(() => {
            // process.exit(0);
        });

    //             var descriptionSplitByCommas = desc.split(",");
    //             Promise.mapSeries(descriptionSplitByCommas, function (name) {   //TODO 4: are you sure mapSeries is the right thing here
    //                 // name = name.toLowerCase();
    //                 // name = common.removeDoubleSpaces(name);
    //                 // name = common.removePercentageNumber(name);
    //                 name = name.trim();

    //                 if (name.length > 0) { // there were cases when name was empty after normalisation, if not for now still leave this "if" statement here
    //                     // console.log('(1) Name = ' + name + '| for Product ' + product.description + '| with id = ' + product.id);
    //                     var normalised_name = common.normaliseForDatabase(name); //todo: this is second call of the same func

    //                     var eNumbers = common.getEnumbersOrLeaveStr(normalised_name);

    //                     var processNames = [normalised_name];
    //                     // if (eNumbers && eNumbers.length > 0) {
    //                     //     processNames = eNumbers;
    //                     // }

    //                     processNames.forEach((normalised_name) => {
    //                         ingredient_get_or_create(normalised_name, name)
    //                             .then(function (ingredient_id) {
    //                                 if (ingredient_id == null || ingredient_id == undefined) return; //do nothing if no ingredient_id

    //                                 // insert it not exists
    //                                 var rawStr = "INSERT INTO product_ingredients (ingredient_id, product_id) SELECT ?, ? WHERE NOT EXISTS(SELECT 1 FROM product_ingredients WHERE ingredient_id = ? and product_id = ?)";
    //                                 return knex.raw(rawStr, [ingredient_id, product.id, ingredient_id, product.id])
    //                                     .on('query', function (data) {
    //                                         // console.log("ON-QUERY data:", data);
    //                                     })
    //                                     .catch(function (err) {
    //                                         console.log('Err Insert where not exists ' + err);
    //                                     });

    //                                 // console.log('    (4) ingr_id = ' + ingredient_id + ' product_id = ' + product.id);                           
    //                             });
    //                     });
    //                 }
    //                 else {
    //                     return;  //TODO 5: are you sure "nothing" should be returned
    //                 };
    //             }
    //         })
    //         .catch(function (err) {
    //             console.log('Err catch ' + err);
    //         });
    // })
    //         .then(function () {
    //     // console.log('Do nothing');
    // })
    //     .catch(function (err) {
    //         // All the error can be checked in this piece of code            
    //         console.log('Err2 catch ' + err);
    //     })
    //     .finally(function () {
    //         // To close the connection pool
    //         // knex.destroy();
    //     });

}
// If ingredient with given name exists, return its id. The comparison is done also in synonyms.
// If ingredient doesn't exist, then create it and return newly created Id. 
// TODO: in future for newly created ingredients, admin would need to clean up - check whether it was really new ingredient or maybe synonim or whatever.
function ingredient_get_or_create(normalised_name, name) {
    return ingredient_get(normalised_name, name)
        .then(function (ingredient_id) {
            // Ingredient found, return
            // console.log('     (3) Ingredient found or not for name = ' + name + '. Found ingr = ' + ingredient_id);
            if (ingredient_id) return ingredient_id;

            //TODO 10: move this to seperate function named “add_ingredient(normalised_name)”
            if (!isNumeric(normalised_name)) {
                // console.log('Do insert = ' + name);
                return knex('ingredients').insert({ name_lv: name, source: 'by_algo', name_lv_normalised: normalised_name })
                    .then((ingredient_id) => ingredient_id)
                    .catch(function (err) {
                        // TODO 7: very often here we gave duplicate key error in db
                        // I believe this is because async calls 
                        // and another product already created INGREDIENT
                        // Currently as quickfix: try one more time to get as maybe it was already inserted because of async calls
                        console.log('Err catched ');
                        console.log(err);
                        return ingredient_get_or_create(name);
                    })
            } else {
                // Ignore names that are purely numeric
                return null; //TODO 9: why null? 
            }
        });

}

// Gets ingredient_id if given ingredient exists (based on original ingredient name and include all synonyms)
// Returns ingredient_id
function ingredient_get(normalised_name, name) {
    var q_ingredients = knex('ingredients')
        .select('id').where('name_lv_normalised', normalised_name)
        .then(pick_id)

    var q_synonyms = knex('synonyms')
        .select('ingredient_id as id').where('synon_name_lv_normalised', normalised_name)
        .then(pick_id)

    return Promise.all([q_ingredients, q_synonyms])
        .then(([id1, id2]) => {
            // console.log('  (2) Ingredient_get for name = ' + name + ' found id1 = ' + id1 + ' and id2 = ' + id2);
            return id1 || id2;
        })
    // FYI:  another approach would be using COALESCE something like:
    // return knex.raw('SELECT COALESCE(?, ?) AS name', [ q_ingredients, q_synonyms ]).then(pick_name)
    // Existing sollution optimizes count of queries to DB.
    // Second form optimizes answer time.            
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function pick_id(rows) {
    if (!rows.length) return null;
    return rows[0].id;
}

// Remove text "var saturēt".
// Assumption 1: this text exists at the end of description and nothing else is afterwards.
// Assumptions 2: this text starts after comma or point  (for instance,"produkta apraksts. Var saturét..." or "produkta apraksts, produkts var saturét")
// Assumption 3: contains "var saturet" or "var ietvert", but there could be some other words before it like "Produkts var..." or "Nelielos daudzumos var ..."

// function descriptionNormalise(product_id, str) {
//     var name = str;



//     // name = common.removePercentageNumber(name); // this should be run before replacing dots to commas            

//     // name = name.replace(/[()]/gi, ','); //replace brackets with commas
//     // name = name.replace(/[:]/gi, ','); //replace : with commas
//     // name = name.replace(/[;]/gi, ','); //replace ; with commas
//     // name = name.replace(/[.]/gi, ','); //replace . with commas    

//     // name = cleanups.replaceNumbersCommasToPeriods(name);


//     name = removeSpaceAfterBeforeBrackets(name);



//     name = processSojasLecitini(name);

//     // remove tsk
//     name = removeTSk(name);

//     // remove e-number cateogries and other special words
//     name = cleanAndSaveSpecialWords(name);

//     name = cleanups.removeDoubleSpaces(name);
//     name = name.trim();


//     // //todo: this could be moved to start of algorithm before even description splitted
//     // // and these data could be collected somwhere
//     // var specialWords = [
//     //     'antioksidant', 
//     //     'konservant', 
//     //     'biezinātāj', 
//     //     'krāsviela', 
//     //     'emulgator', 
//     //     'skābuma regulētāj', 
//     //     'saldinātāj', 
//     //     'stabilizētāj',
//     //     'garšas un aromāta pastiprinātāj'];

//     // // remove special words that doesn't matter
//     // // todo: be sure that these words are not part of ingredients, otherwise sometimes they could be added into synonyms
//     // specialWords.forEach((word) => {
//     //     name = common.removeSpecialWord(name, word);
//     // });

//     // name = name.replace("ar krāsvielu", "");
//     // name = name.replace("ar krāsvielām", "");
//     // name = name.replace("satur ", "");
//     // // name = name.replace("sastāvdaļas", "");
//     // name = name.replace("ieskaitot", "");



//     return name;
// };


function clean_VarSaturet(product_id, desc) {
    var n = desc.toLowerCase().indexOf("var saturēt");
    if (n == -1) {
        n = desc.toLowerCase().indexOf("var ietvert");
    }

    if (n == -1) {
        // do nothing if neither of these text was found
        return desc;
    }

    // get index of comma or period before Var Saturet text
    var dotIndex = desc.lastIndexOf(".", n);
    var commaIndex = desc.lastIndexOf(",", n);

    // take largest (first from end)
    var ind = Math.max(dotIndex, commaIndex);

    var newDesc = desc.substr(0, ind + 1);
    var varSaturetStr = desc.substr(ind + 1, desc.length - ind - 1);

    // Update product, adding "var saturet" information
    updateVarSaturet(product_id, varSaturetStr);

    return newDesc;
}

function removeTSk(str) {
    // izdzēšam tekstu t.sk.

    if (str.substr("t.sk.") == -1 && str.substr("tai skaitā") == -1)
        return desc;

    // t.sk. in lecitíns tsk.sojas processed seperately

    // remove t.sk.    
    str = str.replace(/t\.sk\./gi, ",");
    str = str.replace(/tai skaitā\./gi, ",");

    return str;
}

function process_SojasLecitini(str) {
    // izņēmuma gadījums, apstrādā atsevišķi
    /*
        lecitīni t.sk. sojas
        lecitīns t.sk. sojas
        lecitīns, t.sk. sojas
        lecitīni, t.sk., sojas
        lecitīns , t.sk. , sojas
        ---> sojas lecitīni
    */
    str = str.replace(/lecitīns?i?\s*,?\s*t\.sk\.\s*,?\s*sojas/gi, "sojas lecitīni");

    /* lecitīni(sojas) */
    str = str.replace(/lecitīns?i?\s*\(+\s*sojas\s*\)+\s*/gi, "sojas lecitīni");
    return str;
}

function cleanAndSaveSpecialWords(str) {
    // speciālie vārdi ir bez galotnes, jo meklēšana notiks gan vsk., gan dsk.
    var collection = [];

    // order of words is important here
    var specialWords_eNumberGroups = [
        'antioksidant',
        'dūmu konservant', 'konservantas', 'konservant',
        'pārtikas krāsviela', 'krāsviela',
        'skābuma regulētāj',
        'emulgator',
        'iebiezinātāj', 'biezinātāj', 'biezinataj',
        'saldinātāj',
        'stabilizētāj', 'stabilizator', 'stabilizātor', 'stbilizatori',
        'skābuma regulētāj', 'skābuma regulētaj', 'skabuma regulētāj',
        'irdinātāj',
        "garšas un smaržas pastiprinātāj", "garšas un aromātu pastiprinātāj",
        'garšas pastiprinātāj', 'garša pastiprinātāj', 'garšas pastiprinātaji',
        'smaržas pastiprinātāj',
        'vitamīn'
    ];

    // aromatizētāji?? todo

    specialWords_eNumberGroups.forEach((word) => {
        var regex1 = RegExp(word + '?s?i?\s*:?\s*', 'gi');
        if (regex1.test(str)) {
            collection[word] = 1;
            str = str.replace(regex1, "");
        }
    });

    //todo: something to do with createc collection

    return str;
}

function updateVarSaturet(product_id, varSaturetText) {
    knex('products')
        .update({ var_saturet_text: varSaturetText })
        .where("id", product_id)
        .then(function (data) { });
}

//TODO: take not only (, but also [ brackets
function p(str) {
    //negative lookahead expression, that asserts that we don't have a ) ahead following 0 or more non-round-bracket characters 
    // or don't have a ] ahead following 0 or more non-square-bracket characters.
    var reg = /,\s*(?![^()]*\)|[^\]\[]*\])/;
    return str.split(reg);
}

function no_brackets(str) {
    return str.indexOf('(') == -1;
};

var fn2 = function rec_divide(str) {
    if (no_brackets(str)) {
        return str;
    };
    return p(str);
}

function removeSpaceAfterBeforeBrackets(str) {
    str = str.replace(/\s*\(\s*/gi, "(");
    str = str.replace(/\s*\)\s*/gi, ")");
    return str;
}

// *** Check if character is an opening bracket ***
function isOpenParenthesis(parenthesisChar) {
    for (var j = 0; j < tokens.length; j++) {
        if (tokens[j][0] === parenthesisChar) {
            return true;
        }
    }
    return false;
}

// *** Check if opening bracket matches closing bracket ***
function matches(topOfStack, closedParenthesis) {
    for (var k = 0; k < tokens.length; k++) {
        if (tokens[k][0] === topOfStack && tokens[k][1] === closedParenthesis) {
            return true;
        }
    }
    return false;
}

// *** Checks if item is any sort of paranthesis ***
function isParanthesis(char) {
    var str = '{}[]()';
    if (str.indexOf(char) > -1) {
        return true;
    } else {
        return false;
    }
}

// *** We excute this function upon the event ***
function isBalanced(str) {
    var inputStr = str;
    if (inputStr === null || inputStr == undefined) { return true; }

    var expression = inputStr.split('');
    var stack = [];

    for (var i = 0; i < expression.length; i++) {
        if (isParanthesis(expression[i])) {
            if (isOpenParenthesis(expression[i])) {
                stack.push(expression[i]);
            } else {
                if (stack.length === 0) {
                    return false;
                }
                var top = stack.pop(); // pop off the top element from stack
                if (!matches(top, expression[i])) {
                    return false;
                }
            }
        }
    }

    var returnValue = stack.length === 0 ? true : false;
    return returnValue;
}
var express = require('express');
var app = express();
var knex = require('../db');
var port = process.env.PORT || 3000;
var Promise = require("bluebird");

knex("products").select("products.id as id", "products.name_lv as name", "products.ingredient_description_lv as description")
    .mapSeries(function (product) {
        Promise.mapSeries(product.description.split(","), function (name) {
            var normalised_name = name;
            ingredient_get_or_create(normalised_name, name)
                .then(function (ingredient_id) {
                    return;
                });
        });
    });

// If ingredient with given name exists, return its id. The comparison is done also in synonyms.
// If ingredient doesn't exist, then create it and return newly created Id. 
// TODO: in future for newly created ingredients, admin would need to clean up - check whether it was really new ingredient or maybe synonim or whatever.
function ingredient_get_or_create(normalised_name, name) {
    return ingredient_get(normalised_name, name)
        .then(function (ingredient_id) {
            // Ingredient found, return
            // console.log('     (3) Ingredient found or not for name = ' + name + '. Found ingr = ' + ingredient_id);
            if (ingredient_id) return ingredient_id;
            return knex('ingredients').insert({ name_lv: name, source: 'by_algo', name_lv_normalised: normalised_name })
                .then((ingredient_id) => ingredient_id)
                .catch((err) => {
                    console.log(err);
                });
        });

}

function ingredient_get(normalised_name, name) {
    var q_ingredients = knex('ingredients')
        .select('id').where('name_lv_normalised', normalised_name).then(pick_id)

    var q_synonyms = knex('synonyms')
        .select('ingredient_id as id').where('synon_name_lv_normalised', normalised_name).then(pick_id)

    return Promise.all([q_ingredients, q_synonyms])
        .then(([id1, id2]) => {            
            return id1 || id2;
        });
}

function pick_id(rows) {
    if (!rows.length) return null;
    return rows[0].id;
}
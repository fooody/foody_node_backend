var express = require('express');
var app = express();
var knex = require('./db');
var port = process.env.PORT || 3000;
var bodyParser = require('body-parser');


// Converts row data (in JSON/associative array format or flat array format) to object/tree structure based on simple column naming conventions.
var Treeize = require("treeize");
var Promise = require("bluebird");
var algoHelper = require('./algo_helper');

app.use(express.static('imgs'));



// CATEGORIES
// TODO: move to seperate categoriesRoutes

// var categoryRoutes = require('./routes/categoryRoutes');
// app.use('/api/v1/categories',)

// Get all categories
app.get('/api/v1/categories', function (req, res) {
    knex("categories")
        .select(selectCategoryForApp())
        // .order_by("name_lv")
        .then(function (categories) {
            res.json(categories);
        })
        .catch(function (err) {
            res.status(500);
        })
        .finally(function () {
            //  knex.destroy();
        });
});

// Get one category by id
app.get('/api/v1/categories/:id', function (req, res) {
    knex("categories")
        .first(selectCategoryForApp())
        .where('id', req.params.id)
        .then(function (category) {
            res.json(category);
        })
        .finally(function () {
            //  knex.destroy();
        });
});

// Get direct children categories of given category
// TODO: for now just return direct category-products and do not look into deeper hierarchy
app.get('/api/v1/categories/:id/children', function (req, res) {
    knex("products").select("name_lv as name")
        .where('category_id', req.params.id)
        .then(function (categories) {
            res.json(categories);
        })
        .finally(function () {
            // knex.destroy();
        });
});

// Get direct products in given category
app.get('/api/v1/categories/:id/products', function (req, res) {
    var categoryId = req.params.category_id;
    knex("products")
        .select(selectProductForApp())
        // .select(knex.raw('count(*) OVER() as image'))
        .where("category_id", categoryId)
        .order_by("name_lv")
        .then(function (categories) {
            res.json(products);
        })
        .catch(function (err) {
            return res.sendStatus(500);
        })
        .finally(function () {
            // knex.destroy();
        });
});

// PRODUCTS
// TODO: move to seperate productsRoutes

// Get one product by barCode
app.get('/api/v1/products/:barCode', function (req, res) {
    knex("products")
        //.select("id", "rating as rate", "need_recalc")
        .first("id")
        .where('barcode', req.params.barCode)
        .then(function (product) {
            // if (product.rating == null || product.rating == 0 || product.need_recalc == 1) {
            //     algoHelper.update_product_rating(product.id)
            //         .then((rating) => {
            //             product.rate = rating;
            //             res.json(product);
            //         })
            // } else {
            //     res.json(product);
            // }
            res.json(product);
        })
        .catch((error) => {            
            // console.log(error);
            res.status(500).json({ error });
        })
        .finally(function () {
            // knex.destroy();
        });
});

// Run one product recalculation
// This should not be public API, but in future could be run from admin portal
app.get('/api/v1/products/:barCode/recalculateRating', function (req, res) {
    res.send("Not implemented yet");
});

// Run product recalculation for those products were rating is null
// This should not be public API, but in future could be run from admin portal
app.get('/api/v1/products/recalculateRating', function (req, res) {
    res.send("Not implemented yet");
});

// Get one product by barCode
app.get('/api/v1/products/:barCode/img', function (req, res) {
    // res.json("test");
    res.sendFile(__dirname + '/imgs/' + req.params.barCode + '.png', null, (err) => {
        res.sendFile(__dirname + '/imgs/noimg.png')
    });
    // knex("products")
    //     .first(selectProductForApp())
    //     .where('barcode', req.params.barCode)
    //     .then(function (product) {
    //         if (product.rating == null || product.rating == 0 || product.need_recalc == 1) {
    //             algoHelper.update_product_rating(product.id)
    //                 .then((rating) => {
    //                     product.rate = rating;
    //                     res.json(product);
    //                 })
    //         } else {
    //             res.json(product);
    //         }
    //     })
    //     .catch((error) => {
    //         response.status(500).json({ error });
    //     })
    //     .finally(function () {
    //         // knex.destroy();
    //     });
});



function selectCategoryForApp() {
    return ["id",
        "name_lv as name",
        "image_src as image"];
}

function selectProductForApp() {
    return ["id",
        "barcode as bar_code",
        "name_lv as name",        
        "ingredient_description_lv as description",
        "rating as rate",
        "energy_value as energetics",
        "fat as fat",
        "protein as protein",
        "incl_sugar as sugar",
        "incl_saturated_fat as fatty_acids", //???
        "carbohydrates as carbohydrates",
        "salt as salt",
        "name_lv as components_cleartext", ///???
        "name_lv as issuer_cleartext",
        "need_recalc as need_recalc"];  ///???
}

function recalculateRating(product) {
    return new Promise((res, req) => {
        resolve('resolve'); //todo
    });
}

// HELPER functions 
// FOR ALGOrithm purposes


//TODOs:
// destroy knex
// move routes to seperate routes folders
// move helper functions somewhere




// var categoriesRouter = require('./routes/categoryRoutes')();
// // var productsRouter = require('./routes/productRoutes')();

// app.use('/api/v1/categories', categoriesRouter);
// // app.use('/api/v1/products', productsRouter);




// // clear();

// var pCategories = knex("categories").where("id", 1).debug(false).then();
// var pProducts = knex("products").where("category_id", 1).debug(false).then();
// Promise.all([pCategories, pProducts]).then(function(results) {
//     // var author = results[0];
//     // author.books  = results[1];
// });

// var query = knex.select("name_lv").from("categories");
// // var sql = query.toString();
// // var sql = query.toSQL();

// query.then(function (rows) {
//     console.log(rows);
// })
//     .catch(function (err) {
//         if (err) {
//             console.log(err);
//         }
//     })
//     .finally(function() {
//         knex.destroy();
//     });

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());




// // foodieRouter.route('/products/:barCode')
// //     .get(function (req, res) {
// //         // // http://foodycheck.com/api/v1/get_barcode_info

// //         // req.params.barCode

// //         var responseJson = { hello: "product by bar code" };

// //         // res.send('All categories!');
// //         res.json(responseJson);
// //     });


// // POSTs //todo: move this out of this

// // app.get('/api/v1/products', function(req, res) {
// //     res.send('All products!');
// // });





// // set up get, just send to browser info
// app.get('/', function (req, res) {
//     res.send('Hello World!');
// });


// // http://foodycheck.com/api/v1/get_favorites

// app.get('/api/v1/products', function (req, res) {
//     res.send('All products!');
// });
// // routes = require('./api/routes');

// //  Connect all our routes to our application
// // app.use('/', routes);

// // Turn on that server!

app.listen(port, () => {
    console.log('Gulp is running on PORT: ' + port);
});
update products 
set category_id = (select id from categories where name_lv = 'Cepumi' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Cepumi un biskvīti > Cepumi';

update products 
set category_id = (select id from categories where name_lv = 'Cepumi ar pildījumu' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Cepumi un biskvīti > Cepumi ar pildījumu';

update products 
set category_id = (select id from categories where name_lv = 'Cepumi iepakojumā' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Cepumi un biskvīti > Cepumi iepakojumā';

update products 
set category_id = (select id from categories where name_lv = 'Cepumi iepakojumā' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Cepumi un biskvīti > Cepumu kastes';

update products 
set category_id = (select id from categories where name_lv = 'Citi cepumi' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Cepumi un biskvīti > Citi cepumi';

update products 
set category_id = (select id from categories where name_lv = 'Citi cepumi' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Cepumi un biskvīti > Prjaņiki';

update products 
set category_id = (select id from categories where name_lv = 'Citi cepumi' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Cepumi un biskvīti > Sveramie cepumi';

update products 
set category_id = (select id from categories where name_lv = 'Sāļie cepumi un krekeri' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Cepumi un biskvīti > Sāļie cepumi un krekeri';

update products 
set category_id = (select id from categories where name_lv = 'Citas vafeles' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Vafeles > Citas vafeles';

update products 
set category_id = (select id from categories where name_lv = 'Citas vafeles' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Vafeles > Sveramas vafeles';

update products 
set category_id = (select id from categories where name_lv = 'Vafeles iepakojumā' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Vafeles > Vafeles';

update products 
set category_id = (select id from categories where name_lv = 'Vafeles iepakojumā' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Vafeles > Vafeles iepakojumā';

update products 
set category_id = (select id from categories where name_lv = 'Vafeļu tortes' and parent_id is not null LIMIT 1)
where external_category_path = 'Cepumi un vafeles > Vafeles > Vafeļu tortes';

commit;


insert into products(barcode, external_link, external_name, external_category_path, external_img, manufacturer, country, ingredient_description_lv, external_additional_information, external_reference_intake_description, energy_value, fat, incl_saturated_fat, carbohydrates, incl_sugar, protein, salt, external_ref_intake_kkcal, external_ref_intake_fat, external_ref_intake_saturated, external_ref_intake_carbor, external_ref_intake_sugar, external_ref_intake_protein, external_ref_intake_salt) VALUES (1113311052, 'https://app.rimi.lv/entry/sojas-merce-kikkoman-1l/1113311052', 'Sojas mērce Kikkoman 1l', 'Garšvielas, eļļas, mērces > Etniskie un eksotiskie ēdieni > Austrumu garšvielas un ēdienu sastāvdaļas', 'https://rimibaltic-res.cloudinary.com/image/upload/w_320,h_480,f_auto,q_auto,fl_lossy,c_pad,d_mobile-app:blank.png/MAT_110938_PCE_LV', 'McIlhenny', 'Nīderlande', 'ūdens, SOJAS pupiņas, KVIEŠI, pārtikas ražošanas sāls', 'Soja,Satur glutēnu', 'Ieteicamā deva vidusmēra pieaugušajam (8400 kJ / 2000 kcal). Nepieciešamais uzturvielu daudzums katram indivīdam var būt atšķirīgs atkarībā no dzimuma, vecuma, fiziskās aktivitātes un citiem faktoriem.', '77', '0 g', '0 g', '3.2 g', '0.6 g', '10 g', '16.9 g', '77 / 3%', '0 g / 0%', '0 g / 0%', '3.2 g / 1%', '0.6 g / 0%', '10 g / 20%', '16.9 g / 281%' );

update products
set name_lv = external_name;

update products
set external_source_comment = 'rimi_scrapped_db';

commit;
select * from products;
SET @incorrectIngredientId = 2225
;
SET @newIngredientId = 3258
;

select * from product_ingredients
where ingredient_id = @incorrectIngredientId;

/*select * from product_ingredients
where product_id = 3094 and ingredient_id = @newIngredientId;*/

UPDATE product_ingredients
set ingredient_id = @newIngredientId
where ingredient_id = @incorrectIngredientId 
-- and product_id != 3649 and product_id != 1876
;

delete from product_ingredients
where ingredient_id = @incorrectIngredientId;

DELETE FROM ingredients
where id = @incorrectIngredientId;

commit;

/*CALL add_synonyms(1663,'zivs');
commit;*/

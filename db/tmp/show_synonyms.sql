SET @ingredientId = 2225
;

select * from ingredients ingr
left  join synonyms s
on ingr.id = s.ingredient.id
where ingr.id = @ingredientId
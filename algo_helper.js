var express = require('express');
var app = express();
var knex = require('./db');
var Promise = require("bluebird");


var algoHelper = {
    update_product_rating: function (product_id) {
        return this.calculate_product_rating(product_id)
            .then((rating) => {
                rating = rating == null ? 0 : rating;
                // console.log(product_id + ' = ' + rating);
                    
                return knex('products')
                    .where('id', product_id)
                    .update('rating', rating)
                    .then(() => {                        
                        return rating;
                    });
            })
    },

    // Calculate or recalculate product rating
    calculate_product_rating: function (product_id) {
        // sum up all ratings
        return knex('product_ingredients')
            .join('ingredients', 'ingredients.id', '=', 'product_ingredients.ingredient_id')
            .where("product_ingredients.product_id", product_id)
            .where("ingredients.affect_algorithm", 1)
            .sum('ingredients.rating_adjustments as adjustment')
            .then(function (adjustments) {
                if (adjustments != null) {
                    return adjustments[0]["adjustment"];
                }
                return adjustments; //null
            });
    }
};
module.exports = algoHelper;
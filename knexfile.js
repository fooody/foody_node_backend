module.exports = 
{    
    client: "mysql",
    connection: {
        //host: 'localhost',
        host: 'homestead',
        user: 'root',
        password: 'SuperSecretRootPassword',
        database: 'foodiedb'
    },
    debug: true,
     pool: {min: 1, max: 100}    
};
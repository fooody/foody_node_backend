#New Backend of FoodyCheck

Cloned from Public Repo (https://bitbucket.org/foodycheck/foody-check-backend/commits/all)

![open source project with full database](https://bytebucket.org/fooody/foody_node_backend/raw/21dd6a243afb3c1612712aa9f1ba0c5526fa7c7a/docs/suecide-kill.gif)

To Run Install Docker + Docker Compose and `docker-compose up`

Import DB from Mysql Docker instance run /opt/foodycheck/import.sh

To view site in Adminer http://localhost:86 using creditals from .mysql.env

Works almost compleatly with [Foody V1 API Documentatiob](https://gist.github.com/shivergard/b3a2d95cf702a04ea8b6743151d3f2b6) :D

Interesting [Product Dataset](https://bitbucket.org/fooody/foody_node_backend/raw/ba651a225e7d484559cd0d985416636e3f8a8b2e/docs/products_foody_v2.csv) from [Database Dump](https://bitbucket.org/fooody/foody_node_backend/src/c9df3b378d6deeee3bedc9b7a7ca035dd51d7f9e/db/foodie.sql?at=master&fileviewer=file-view-default) 
